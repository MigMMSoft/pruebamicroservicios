package com.mmsoft.api.gateway;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @author MigSoft
 */
@EnableZuulProxy
@EnableDiscoveryClient
@SpringBootApplication
@RestController
public class ApiGatewayApplication {
    
    /**
     * 
     * @param args 
     */
    public static void main(String[] args) {
        SpringApplication.run(ApiGatewayApplication.class, args);
    }

    @RequestMapping("/")
    public String home() {
        return "Micro Servicio APIGateway desarrollado con spring-boot";
    }
}

