# Implementaci&oacute;n de Api Gateway

Este proyecto tiene la inplementacion de Zuul. Como Api Gateway

Zuul is an edge service that provides dynamic routing, monitoring, resiliency, security, and more.
Please view the wiki for usage, information, HOWTO, etc https://github.com/Netflix/zuul/wiki


### Detalle del sistema

- Java 11
- Spring Boot 2.5.14
- Maven 3

### Compilacion y ejecucion

```bash
mvn clean package
```

Genera en el directorio targer la **APIGateway-x.x.x**

Ejecutar servidor con 

```bash
java -jar APIGateway-x.x.x.jar &
```

## Crear imagenes y pod 

Una vez compilado se crea la imagen docker y se publica

```
$ docker image rm -f mmsoft.api_gateway:1.0.0

$ docker build -t mmsoft.api_gateway:2.0.0 ./

$ docker image ls|grep mmsoft
```
### Publica pod en el kubernetes

Genera el pod

```
$ kubectl apply -f deployment.yaml
service/api-gateway created
statefulset.apps/api-gateway created
```

ver pods

```
$ kubectl get pods
```
## Notas

Ver errores

```
$ kubectl describe pod api-gateway-0
$ kubectl logs api-gateway-0
```

## Eliminar despliegue en kubernetes

```
$ kubectl delete statefulset api-gateway
$ kubectl delete service api-gateway
```


### Detalle del Servicio

La base de todos los servicios a consumir es:

http://localhost:30080/
