# Servicio Notas

Servicio encargo de crear, actualizar, consultar y eliminar una nota.


### Detalle del sistemas

- Swagger 2.0
- Java 11
- Spring-Boot 2.5.2
- Maven 3
- Hibernate 4
- DB H2

### Compilacion y ejecucion

```bash
mvn clean package
```

Genera en el directorio targer la **api_notas-xxx.jar**

Ejecutar servidor con 

```bash
java -jar api_notas-xxx.jar &
```

### Referencias

The underlying library integrating swagger to SpringBoot is [springfox](https://github.com/springfox/springfox)  

Start your server as an simple java application  

You can view the api documentation in swagger-ui by pointing to  
http://localhost:8080/  

Change default port value in application.properties