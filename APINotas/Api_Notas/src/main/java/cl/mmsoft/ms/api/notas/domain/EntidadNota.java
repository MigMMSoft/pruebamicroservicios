package cl.mmsoft.ms.api.notas.domain;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;


@Entity
@Table(name = "nota")
@Getter @Setter
public class EntidadNota extends AbstractEntity<String>{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "titulo", length = 100)
    private String titulo;
    
    @Column(name = "comentario", length = 100)
    private String comentario;
}
