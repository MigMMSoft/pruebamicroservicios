package cl.mmsoft.ms.api.notas.swagger.api;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

import ch.qos.logback.core.pattern.parser.Node;
import cl.mmsoft.ms.api.notas.services.NotaServiceImpl;
import cl.mmsoft.ms.api.notas.services.SinDatoException;
import cl.mmsoft.ms.api.notas.swagger.model.Nota;
import io.swagger.annotations.ApiParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import java.util.Optional;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class NotaApiController implements NotaApi {

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @Autowired
    private NotaServiceImpl notaService;

    @org.springframework.beans.factory.annotation.Autowired
    public NotaApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    @Override
    public Optional<ObjectMapper> getObjectMapper() {
        return Optional.ofNullable(objectMapper);
    }

    @Override
    public Optional<HttpServletRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    @Override
    public ResponseEntity<Void> actualizarNota(@ApiParam(value = "Nota que va a ser actualizado en el sistema" ,required=true )  @Valid @RequestBody Nota body) {
        if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
            try{
                notaService.update(body);
                return new ResponseEntity<>(HttpStatus.OK);
            }catch(SinDatoException e){
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST); 
            }catch(Exception e){
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
            
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default NotaApi interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @Override
    public ResponseEntity<Void> eliminarNota(@NotNull @ApiParam(value = "Id del Nota a eliminar", required = true) @Valid @RequestParam(value = "id", required = true) Long id) {
        if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
            try{
                notaService.delete(id);
                return new ResponseEntity<>(HttpStatus.OK);
            }catch(SinDatoException e){
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST); 
            }catch(Exception e){
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default NotaApi interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @Override
    public ResponseEntity<Void> ingresarNota(@ApiParam(value = "Nota que va a ser agregada al sistema" ,required=true )  @Valid @RequestBody Nota body) {
        if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
            try{
                notaService.saveNota(body);
                return new ResponseEntity<>(HttpStatus.OK);
            }catch(Exception e){
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default NotaApi interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @Override
    public ResponseEntity<Nota> verNota(@NotNull @ApiParam(value = "Id del Nota a eliminar", required = true) @Valid @RequestParam(value = "id", required = true) Long id) {
        if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
            if (getAcceptHeader().get().contains("application/json")) {
                try {
                    return new ResponseEntity<>(notaService.getNota(id), HttpStatus.OK);
                } catch (SinDatoException e) {
                    log.error("Couldn't serialize response for content type application/json", e);
                    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default NotaApi interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
