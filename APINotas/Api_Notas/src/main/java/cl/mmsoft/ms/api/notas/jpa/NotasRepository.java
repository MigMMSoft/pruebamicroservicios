package cl.mmsoft.ms.api.notas.jpa;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cl.mmsoft.ms.api.notas.domain.EntidadNota;

@Repository
public interface NotasRepository extends CrudRepository<EntidadNota, Long>{
    @Query( "select f from EntidadNota f" )
    public Page<EntidadNota> findAllCustom( Pageable pageable);
}
