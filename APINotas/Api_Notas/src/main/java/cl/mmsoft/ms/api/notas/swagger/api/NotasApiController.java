package cl.mmsoft.ms.api.notas.swagger.api;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;

import cl.mmsoft.ms.api.notas.services.NotaServiceImpl;
import cl.mmsoft.ms.api.notas.swagger.model.Notas;
import io.swagger.annotations.ApiParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import java.util.Optional;

@Controller
public class NotasApiController implements NotasApi {

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @Autowired
    private NotaServiceImpl notaService;

    @org.springframework.beans.factory.annotation.Autowired
    public NotasApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    @Override
    public Optional<ObjectMapper> getObjectMapper() {
        return Optional.ofNullable(objectMapper);
    }

    @Override
    public Optional<HttpServletRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    public ResponseEntity<Notas> verNotas(@ApiParam(value = "Pagina actual") @Valid @RequestParam(value = "pagina", required = false) Integer pagina,@ApiParam(value = "Cantidad de elementos en la pagina") @Valid @RequestParam(value = "max", required = false) Integer max) {
        if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
            if (getAcceptHeader().get().contains("application/json")) {
                try {
                    return new ResponseEntity<>(notaService.getNotas(pagina, max), HttpStatus.OK);
                } catch (Exception e) {
                    log.error("Couldn't serialize response for content type application/json", e);
                    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default NotasApi interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

}
