package cl.mmsoft.ms.api.notas.services;

public class SinDatoException extends Exception {
    public SinDatoException(String mensaje){
        super(mensaje);
    }
}
