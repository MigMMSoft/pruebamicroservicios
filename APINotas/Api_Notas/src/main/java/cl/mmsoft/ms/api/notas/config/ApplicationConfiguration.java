package cl.mmsoft.ms.api.notas.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = {"cl.mmsoft.ms.api.notas.jpa"})
@EntityScan("cl.mmsoft.ms.api.notas.domain")
public class ApplicationConfiguration {
}
