package cl.mmsoft.ms.api.notas.services;

import cl.mmsoft.ms.api.notas.swagger.model.Nota;
import cl.mmsoft.ms.api.notas.swagger.model.Notas;

import java.io.Serializable;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import cl.mmsoft.ms.api.notas.domain.EntidadNota;
import cl.mmsoft.ms.api.notas.jpa.NotasRepository;

@Service
public class NotaServiceImpl implements Serializable{
    private static final Logger log = LoggerFactory.getLogger(NotaServiceImpl.class); 

    @Autowired
    private NotasRepository notaRepository;

    private ZoneOffset zoneOffset  = ZoneOffset.ofHours(-4);

    public Nota getNota(long notaId) throws SinDatoException{
        log.info("Rescatando nota " + notaId);
        Optional<EntidadNota> notaEntity = notaRepository.findById(notaId);
        if(notaEntity.isPresent()){
            Nota nota = new Nota();
            OffsetDateTime fechaCreacion = null;
            if(notaEntity.get().getFechaCreacion() != null){
                fechaCreacion = notaEntity.get().getFechaCreacion().toInstant().atOffset(zoneOffset);
            }            
            nota.setFechaCreacion(fechaCreacion);
            OffsetDateTime fechaActualizacion = null;
            if(notaEntity.get().getFechaActualizacion() != null){
                fechaActualizacion = notaEntity.get().getFechaActualizacion().toInstant().atOffset(zoneOffset);
            }
            nota.setFechaActualizacion(fechaActualizacion);
            
            nota.setContenido(notaEntity.get().getComentario());
            
            nota.setId(notaEntity.get().getId());
            nota.setTitulo(notaEntity.get().getTitulo());
            return nota;
        }else{
            throw new SinDatoException("No existe " + notaId);
        }
    }

    @Transactional
    public void saveNota(Nota nota){
        EntidadNota notaEntity = new EntidadNota();
        notaEntity.setTitulo(nota.getTitulo());
        notaEntity.setComentario(nota.getContenido());
        notaRepository.save(notaEntity);
    }

    @Transactional
    public void update(Nota nota) throws SinDatoException{
        Optional<EntidadNota> notaEntityOpt = notaRepository.findById(nota.getId());
        if(notaEntityOpt.isPresent()){
            EntidadNota notaEntity = notaEntityOpt.get();
            notaEntity.setTitulo(nota.getTitulo());
            notaEntity.setComentario(nota.getContenido());
            notaRepository.save(notaEntity);
        }else{
            throw new SinDatoException("No existe " + nota.getId() + "para update");
        }
    }

    @Transactional
    public void delete(Long notaId) throws SinDatoException{
        Optional<EntidadNota> notaEntity = notaRepository.findById(notaId);
        if(notaEntity.isPresent()){
            notaRepository.delete(notaEntity.get());
        }else{
            throw new SinDatoException("No existe " + notaId + "para eliminar");
        }
    }

    public Notas getNotas(int pagina, int max){
        Pageable pageable = PageRequest.of(pagina, max, Sort.by("id"));
        Page<EntidadNota> entidadesNotas = notaRepository.
            findAllCustom(pageable);
        Notas notas = new Notas();
        notas.setTotalElementos(entidadesNotas.getTotalElements());
        notas.setTotalPaginas(entidadesNotas.getTotalPages());
        List elementos = new ArrayList();
        entidadesNotas.forEach(notaEntity -> {
            Nota nota = new Nota();
            nota.setId(notaEntity.getId());
            nota.setContenido(notaEntity.getComentario());
            nota.setTitulo(notaEntity.getTitulo());
            OffsetDateTime fechaCreacion = null;
            if(notaEntity.getFechaCreacion() != null){
                fechaCreacion = notaEntity.getFechaCreacion().toInstant().atOffset(zoneOffset);
            }
            nota.setFechaCreacion(fechaCreacion);
            OffsetDateTime fechaActualizacion = null;
            if(notaEntity.getFechaActualizacion() != null){
                fechaActualizacion = notaEntity.getFechaActualizacion().toInstant().atOffset(zoneOffset);
            }
            nota.setFechaActualizacion(fechaActualizacion);
            elementos.add(nota);
        });
        notas.setElementos(elementos);
        return notas;
    }
}
