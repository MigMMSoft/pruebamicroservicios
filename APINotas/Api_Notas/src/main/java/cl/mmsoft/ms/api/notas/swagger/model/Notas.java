package cl.mmsoft.ms.api.notas.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Notas
 */
@Validated




public class Notas   {
  @JsonProperty("totalElementos")
  private Long totalElementos = null;

  @JsonProperty("totalPaginas")
  private Integer totalPaginas = null;

  @JsonProperty("elementos")
  @Valid
  private List<Object> elementos = null;

  public Notas totalElementos(Long totalElementos) {
    this.totalElementos = totalElementos;
    return this;
  }

  /**
   * Get totalElementos
   * @return totalElementos
  **/
  @ApiModelProperty(value = "")


  public Long getTotalElementos() {
    return totalElementos;
  }

  public void setTotalElementos(Long totalElementos) {
    this.totalElementos = totalElementos;
  }

  public Notas totalPaginas(Integer totalPaginas) {
    this.totalPaginas = totalPaginas;
    return this;
  }

  /**
   * Get totalPaginas
   * @return totalPaginas
  **/
  @ApiModelProperty(value = "")


  public Integer getTotalPaginas() {
    return totalPaginas;
  }

  public void setTotalPaginas(Integer totalPaginas) {
    this.totalPaginas = totalPaginas;
  }

  public Notas elementos(List<Object> elementos) {
    this.elementos = elementos;
    return this;
  }

  public Notas addElementosItem(Object elementosItem) {
    if (this.elementos == null) {
      this.elementos = new ArrayList<>();
    }
    this.elementos.add(elementosItem);
    return this;
  }

  /**
   * Get elementos
   * @return elementos
  **/
  @ApiModelProperty(value = "")


  public List<Object> getElementos() {
    return elementos;
  }

  public void setElementos(List<Object> elementos) {
    this.elementos = elementos;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Notas notas = (Notas) o;
    return Objects.equals(this.totalElementos, notas.totalElementos) &&
        Objects.equals(this.totalPaginas, notas.totalPaginas) &&
        Objects.equals(this.elementos, notas.elementos);
  }

  @Override
  public int hashCode() {
    return Objects.hash(totalElementos, totalPaginas, elementos);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Notas {\n");
    
    sb.append("    totalElementos: ").append(toIndentedString(totalElementos)).append("\n");
    sb.append("    totalPaginas: ").append(toIndentedString(totalPaginas)).append("\n");
    sb.append("    elementos: ").append(toIndentedString(elementos)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

