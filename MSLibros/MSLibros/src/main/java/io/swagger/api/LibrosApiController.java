package io.swagger.api;

import io.swagger.model.Libro;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mmsoft.prueba.service.LibroServiceImpl;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-02-18T11:40:46.764-03:00")

@Controller
public class LibrosApiController implements LibrosApi {

    private static final Logger log = LoggerFactory.getLogger(LibrosApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;
    
    @Autowired
    private LibroServiceImpl libroService;

    @org.springframework.beans.factory.annotation.Autowired
    public LibrosApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    @Override
    public ResponseEntity<List<Libro>> libros(@ApiParam(value = "pagina de cosulta", defaultValue = "1") @Valid @RequestParam(value = "offset", required = false, defaultValue="1") Integer offset,@ApiParam(value = "cantidad de registros en la respuesta", defaultValue = "10") @Valid @RequestParam(value = "max", required = false, defaultValue="10") Integer max) {
        String accept = request.getHeader("Accept");
//        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<Libro>>(libroService.getLibros(offset, max), HttpStatus.OK);
            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<Libro>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
//        }
//
//        return new ResponseEntity<List<Libro>>(HttpStatus.NOT_IMPLEMENTED);
    }

}
