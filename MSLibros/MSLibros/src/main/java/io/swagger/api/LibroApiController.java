package io.swagger.api;

import io.swagger.model.Libro;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mmsoft.prueba.service.LibroServiceImpl;
import com.mmsoft.prueba.service.excepciones.SinDatosException;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-02-17T23:34:45.255-03:00")

@Controller
public class LibroApiController implements LibroApi {

    private static final Logger log = LoggerFactory.getLogger(LibroApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @Autowired
    private LibroServiceImpl libroService;

    @org.springframework.beans.factory.annotation.Autowired
    public LibroApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<Libro> libroGet(@ApiParam(value = "ID de usuario", required = true) @PathVariable("id") Long id) {
        String accept = request.getHeader("Accept");
//        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<Libro>(libroService.getLibro(id), HttpStatus.OK);
            } catch(SinDatosException sd){
                return new ResponseEntity<Libro>(HttpStatus.BAD_REQUEST);
            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Libro>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
    }

    @Override
    public ResponseEntity<Void> libroPost(@ApiParam(value = "Libro a actualizar", required = true) @Valid @RequestBody Libro libro) {
        String accept = request.getHeader("Accept");
        try {
            libroService.saveLibro(libro);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (Exception e) {
            log.error("Couldn't serialize response for content type application/json", e);
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<Void> libroPut(@ApiParam(value = "Nuevo Libro", required = true) @Valid @RequestBody Libro libro) {
        String accept = request.getHeader("Accept");
        try {
            libroService.updateLibro(libro);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (Exception e) {
            log.error("Couldn't serialize response for content type application/json", e);
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
