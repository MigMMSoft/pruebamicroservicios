package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.threeten.bp.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Publicacion
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-02-26T22:38:27.142-03:00")

public class Publicacion   {
  @JsonProperty("id")
  private Long id = null;

  @JsonProperty("orden")
  private Long orden = null;

  @JsonProperty("fechaCreacion")
  private OffsetDateTime fechaCreacion = null;

  @JsonProperty("descripcion")
  private String descripcion = null;

  public Publicacion id(Long id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Publicacion orden(Long orden) {
    this.orden = orden;
    return this;
  }

   /**
   * Get orden
   * @return orden
  **/
  @ApiModelProperty(value = "")


  public Long getOrden() {
    return orden;
  }

  public void setOrden(Long orden) {
    this.orden = orden;
  }

  public Publicacion fechaCreacion(OffsetDateTime fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
    return this;
  }

   /**
   * Get fechaCreacion
   * @return fechaCreacion
  **/
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getFechaCreacion() {
    return fechaCreacion;
  }

  public void setFechaCreacion(OffsetDateTime fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

  public Publicacion descripcion(String descripcion) {
    this.descripcion = descripcion;
    return this;
  }

   /**
   * Get descripcion
   * @return descripcion
  **/
  @ApiModelProperty(value = "")


  public String getDescripcion() {
    return descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Publicacion publicacion = (Publicacion) o;
    return Objects.equals(this.id, publicacion.id) &&
        Objects.equals(this.orden, publicacion.orden) &&
        Objects.equals(this.fechaCreacion, publicacion.fechaCreacion) &&
        Objects.equals(this.descripcion, publicacion.descripcion);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, orden, fechaCreacion, descripcion);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Publicacion {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    orden: ").append(toIndentedString(orden)).append("\n");
    sb.append("    fechaCreacion: ").append(toIndentedString(fechaCreacion)).append("\n");
    sb.append("    descripcion: ").append(toIndentedString(descripcion)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

