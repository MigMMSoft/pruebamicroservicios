package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.Publicacion;
import java.util.ArrayList;
import java.util.List;
import org.threeten.bp.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Libro
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-02-26T22:38:27.142-03:00")

public class Libro   {
  @JsonProperty("id")
  private Long id = null;

  @JsonProperty("nombre")
  private String nombre = null;

  @JsonProperty("estilo")
  private String estilo = null;

  @JsonProperty("fechaCreacion")
  private OffsetDateTime fechaCreacion = null;

  @JsonProperty("autor")
  private String autor = null;

  @JsonProperty("nacionalidad")
  private String nacionalidad = null;

  @JsonProperty("publicaciones")
  @Valid
  private List<Publicacion> publicaciones = null;

  public Libro id(Long id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Libro nombre(String nombre) {
    this.nombre = nombre;
    return this;
  }

   /**
   * Get nombre
   * @return nombre
  **/
  @ApiModelProperty(value = "")


  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public Libro estilo(String estilo) {
    this.estilo = estilo;
    return this;
  }

   /**
   * Get estilo
   * @return estilo
  **/
  @ApiModelProperty(value = "")


  public String getEstilo() {
    return estilo;
  }

  public void setEstilo(String estilo) {
    this.estilo = estilo;
  }

  public Libro fechaCreacion(OffsetDateTime fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
    return this;
  }

   /**
   * Get fechaCreacion
   * @return fechaCreacion
  **/
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getFechaCreacion() {
    return fechaCreacion;
  }

  public void setFechaCreacion(OffsetDateTime fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

  public Libro autor(String autor) {
    this.autor = autor;
    return this;
  }

   /**
   * Get autor
   * @return autor
  **/
  @ApiModelProperty(value = "")


  public String getAutor() {
    return autor;
  }

  public void setAutor(String autor) {
    this.autor = autor;
  }

  public Libro nacionalidad(String nacionalidad) {
    this.nacionalidad = nacionalidad;
    return this;
  }

   /**
   * Get nacionalidad
   * @return nacionalidad
  **/
  @ApiModelProperty(value = "")


  public String getNacionalidad() {
    return nacionalidad;
  }

  public void setNacionalidad(String nacionalidad) {
    this.nacionalidad = nacionalidad;
  }

  public Libro publicaciones(List<Publicacion> publicaciones) {
    this.publicaciones = publicaciones;
    return this;
  }

  public Libro addPublicacionesItem(Publicacion publicacionesItem) {
    if (this.publicaciones == null) {
      this.publicaciones = new ArrayList<Publicacion>();
    }
    this.publicaciones.add(publicacionesItem);
    return this;
  }

   /**
   * Get publicaciones
   * @return publicaciones
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<Publicacion> getPublicaciones() {
    return publicaciones;
  }

  public void setPublicaciones(List<Publicacion> publicaciones) {
    this.publicaciones = publicaciones;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Libro libro = (Libro) o;
    return Objects.equals(this.id, libro.id) &&
        Objects.equals(this.nombre, libro.nombre) &&
        Objects.equals(this.estilo, libro.estilo) &&
        Objects.equals(this.fechaCreacion, libro.fechaCreacion) &&
        Objects.equals(this.autor, libro.autor) &&
        Objects.equals(this.nacionalidad, libro.nacionalidad) &&
        Objects.equals(this.publicaciones, libro.publicaciones);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, nombre, estilo, fechaCreacion, autor, nacionalidad, publicaciones);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Libro {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    nombre: ").append(toIndentedString(nombre)).append("\n");
    sb.append("    estilo: ").append(toIndentedString(estilo)).append("\n");
    sb.append("    fechaCreacion: ").append(toIndentedString(fechaCreacion)).append("\n");
    sb.append("    autor: ").append(toIndentedString(autor)).append("\n");
    sb.append("    nacionalidad: ").append(toIndentedString(nacionalidad)).append("\n");
    sb.append("    publicaciones: ").append(toIndentedString(publicaciones)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

