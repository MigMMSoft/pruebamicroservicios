package com.mmsoft.prueba.service;

import com.mmsoft.prueba.domain.Publicacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MigSoft
 */
@Repository
public interface PublicacionRepository extends JpaRepository<Publicacion, Long>{
    
}
