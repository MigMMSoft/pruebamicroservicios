package com.mmsoft.prueba.service;

import com.mmsoft.prueba.domain.Autor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MigSoft
 */
@Repository
public interface AutorRepository extends JpaRepository<Autor, Long>{
    
}
