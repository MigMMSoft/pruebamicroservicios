package com.mmsoft.prueba.config;

import java.util.stream.Stream;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import org.threeten.bp.OffsetDateTime;

import com.mmsoft.prueba.service.LibroServiceImpl;
import io.swagger.model.Libro;
import io.swagger.model.Publicacion;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Inicializacion se encarga de cargar todos los datos por defecto de las tablas 
 * tipo y datos iniciales.
 * @author MigSoft
 */
@Configuration
@EntityScan(basePackages = {"com.mmsoft.prueba.domain"})
@EnableJpaRepositories("com.mmsoft.prueba.service")
public class Inicializacion implements ApplicationRunner{
    
    @Autowired
    private LibroServiceImpl libroService;
    
    /**
     * Carga deatos por defecto
     * @param args 
     */
    @Override
    public void run(ApplicationArguments args) {
        if(libroService.getLibrosCount() == 0){
            Stream.of("Arthur Conan Doyle", "george orwell", "Louisa May Alcott", "Dan Simmons",
                    "Gabriel García Márquez", "Mario Vargas Llosa", "Mario Benedetti", "Paulo Coelho",
                    "Jorge Luis Borges", "Charles Dickens", "Federico García Lorca", "Isabel Allende"
                        )
                    .forEach((String x) -> {
                        Libro l = new Libro();
                        l.setNombre("Libro");
                        l.setEstilo("Indefinido");
                        l.setFechaCreacion(OffsetDateTime.parse("2017-02-13T12:34:56.789+10:00"));
                        l.setAutor(x);
                        l.setNacionalidad("Extrangero");

                        Publicacion p1 = new Publicacion();
                        p1.setOrden(1L);
                        p1.setDescripcion("Publicacion 1");
                        p1.setFechaCreacion(OffsetDateTime.parse("2017-02-15T12:34:56.789+10:00"));
                        l.addPublicacionesItem(p1);

                        Publicacion p2 = new Publicacion();
                        p2.setOrden(2L);
                        p2.setDescripcion("Publicacion 2");
                        p2.setFechaCreacion(OffsetDateTime.parse("2017-02-17T12:34:56.789+10:00"));
                        l.addPublicacionesItem(p2);

                        libroService.saveLibro(l);
                    });
        }
    }
}

