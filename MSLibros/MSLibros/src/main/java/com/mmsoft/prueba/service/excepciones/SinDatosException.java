package com.mmsoft.prueba.service.excepciones;

/**
 *
 * @author MigSoft
 */
public class SinDatosException extends Exception{
    public SinDatosException(){
        super("No se encontraron datos");
    }
}
