package com.mmsoft.prueba.config

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 *
 * @author MigSoft
 */
@EnableAsync
@SpringBootApplication
@EnableJpaAuditing
class AuditingConfiguration {
	
}

