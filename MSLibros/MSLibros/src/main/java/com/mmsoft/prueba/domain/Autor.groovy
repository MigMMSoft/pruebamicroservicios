package com.mmsoft.prueba.domain

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table
import javax.persistence.OneToMany
import javax.persistence.CascadeType
import javax.validation.constraints.NotNull

/**
 *
 * @author MigSoft
 */
@Entity(name = "Autor")
@Table(name = "autor")
class Autor {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id
    @NotNull
    Long version
    
    String nombre 
    String nacionalidad
    
    @OneToMany(mappedBy = "autor", cascade = CascadeType.ALL)
    Set<Libro> libros
}

