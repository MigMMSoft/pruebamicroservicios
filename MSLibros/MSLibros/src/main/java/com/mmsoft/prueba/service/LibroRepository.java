package com.mmsoft.prueba.service;

import com.mmsoft.prueba.domain.Libro;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author MigSoft
 */
@Repository
public interface LibroRepository extends JpaRepository<Libro, Long>{
    @Query( "SELECT f FROM Libro f" )
    Page<Libro> findAllCustom( Pageable pageable);
    
    @Query("SELECT COUNT(u) FROM Libro u")
    Long countLibros();
}
