package com.mmsoft.prueba.service;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author MigSoft
 */
@Service
public class LibroService {
    @Autowired(required=true)
    LibroRepository libroRepository;
    
    @Autowired(required=true)
    AutorRepository autorRepository;
    
    @Autowired(required=true)
    PublicacionRepository publicacionRepository;
}
