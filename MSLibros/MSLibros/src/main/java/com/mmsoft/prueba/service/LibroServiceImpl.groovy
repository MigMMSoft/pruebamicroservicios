package com.mmsoft.prueba.service

import com.mmsoft.prueba.domain.Autor
import com.mmsoft.prueba.domain.Libro
import com.mmsoft.prueba.domain.Publicacion
import com.mmsoft.prueba.service.excepciones.SinDatosException
import org.springframework.stereotype.Service

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List
import java.util.stream.Collectors
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.persistence.Query
import org.springframework.data.domain.Sort;
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

import org.threeten.bp.DateTimeUtils
import org.threeten.bp.ZoneOffset

/**
 *
 * @author MigSoft
 */
@Service
@Transactional(readOnly = true)
class LibroServiceImpl extends LibroService{
    private static final Logger log = LoggerFactory.getLogger(LibroServiceImpl.class)
    
    /**
     * Rescata la cantidad de elementos que tiene Libros
     **/
    int getLibrosCount(){
        log.info("Cuenta elementos de Libro")
        libroRepository.countLibros()
    }

    /**
     * Rescata la lista de libros existente paginado
     * @param offset valor de la pagina a rescatar
     * @param max cantidad de elementos  de la pagina
     **/
    List<io.swagger.model.Libro> getLibros(Integer offset, Integer max) {
        log.info("Rescata la lista de de Libro (max:$max, offset:$offset)")
        def librosDominio = libroRepository.findAllCustom(new PageRequest( offset, max, Sort.Direction.DESC, "id" ))
        
        List<io.swagger.model.Libro> libros = []
        librosDominio.each(){libro->  
            libros << getLibro(libro)
        }
        
        libros
    }

    /**
     * Rescata un libro a partir de un id
     * @param id id a rescatar
     **/
    io.swagger.model.Libro getLibro(Long id)throws SinDatosException {
        log.info("Rescata un libro por Id $id")
        Libro libro = libroRepository.getOne(notaId)
        if(!libro){
            throw new SinDatosException()
        }
       
        getLibro(libro)
    }

    /**
     * Agrega un libro nuevo
     * @param l Libro JSON a ingresar
     **/
    @Transactional
    saveLibro(io.swagger.model.Libro l) {
        log.info("Agrega el Libro ${l?.nombre}")
        def publicaciones = []
        l?.publicaciones?.each(){p->
            Publicacion pub = new Publicacion(version:0,
                                        orden:p.orden,
                                        fecha:DateTimeUtils.toDate(p?.fechaCreacion?.toInstant()))
            
            publicacionRepository.saveAndFlush(pub)
            publicaciones << pub
        }
        def autor = new Autor(version:0,
                        nombre:l.autor,
                        nacionalidad:l.nacionalidad)
        autorRepository.saveAndFlush(autor)
                    
        Libro libro = new Libro(version:0,
                            nombre:l.nombre,
                            estilo:l.estilo,
                            fechaCreacion:DateTimeUtils.toDate(l?.fechaCreacion?.toInstant()),
                            publicaciones:publicaciones,
                            autor:autor);

        libroRepository.saveAndFlush(libro)
    }
    
    /**
     * Actualiza un libro existente
     * @param l Libro JSON a actualizar
     **/
    @Transactional
    updateLibro(io.swagger.model.Libro l) {
        log.info("Actualiza el Libro ${l?.id}")
        Libro libro = libroRepository.getOne(l.id)
        if(libro){
            l?.publicaciones?.each(){p->
                //Actualiza Publicacion
                Publicacion pub = publicacionRepository.getOne(l.id)
                if(pub){
                    pub.version=pub.version++
                    pub.orden=p.orden
                    pub.fecha=DateTimeUtils.toDate(p?.fechaCreacion?.toInstant())
                    publicacionRepository.saveAndFlush(pub)
                }else{
                    throw new Exception("No existe Publicacion id ${p.id} para actualizar");
                }
            }
            //Elimina Publicacion
            libro.publicaciones.each(){p->
                if(l?.publicaciones.id.findIndexValues{ it == p.id}){
                    libro.publicaciones.removeElement(p)
                }
            }
            
            Autor autor = libro.autor
            autor.version=autor.version++
            autor.nombre=l.autor
            autor.nacionalidad=l.nacionalidad

            autorRepository.saveAndFlush(autor)
            
            libro.version = libro.version++
            libro.nombre=l.nombre
            libro.estilo=l.estilo
            libro.fechaCreacion=DateTimeUtils.toDate(l?.fechaCreacion?.toInstant())

            libroRepository.saveAndFlush(libro)
        }else{
            throw new Exception("No existe libro id ${l.id} para actualizar");
        }
    }
    
    /**
    *Convierte un dominio com.mmsoft.prueba.domain.Libro a un objeto JSON
    *io.swagger.model.Libro
    *@param libro Dominio de Libro 
    **/
    io.swagger.model.Libro getLibro(libro){
        def p = libro?.publicaciones?.last()
        def publicacion = 
            new io.swagger.model.Publicacion(id:p.id,
                                             orden:p.orden,
                                             fechaCreacion:DateTimeUtils.toInstant(p.fecha).atOffset(ZoneOffset.UTC))
        def lib = new io.swagger.model.Libro(id:libro.id,
                                nombre:libro?.nombre,
                                estilo:libro?.estilo,
                                fechaCreacion:DateTimeUtils.toInstant(libro?.fechaCreacion).atOffset(ZoneOffset.UTC),
                                autor:libro?.autor?.nombre,
                                nacionalidad:libro?.autor?.nacionalidad)
        lib.addPublicacionesItem(publicacion)
        lib
    }
}
