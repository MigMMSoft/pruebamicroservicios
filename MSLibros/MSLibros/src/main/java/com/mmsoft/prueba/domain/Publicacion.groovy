package com.mmsoft.prueba.domain

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table
import javax.persistence.ManyToMany
import javax.validation.constraints.NotNull

/**
 *
 * @author MigSoft
 */
@Entity(name = "Publicacion")
@Table(name = "publicacion")
class Publicacion {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id
    @NotNull
    Long version
    
    int orden
    Date fecha
    
    @ManyToMany(mappedBy = "publicaciones")
    Set<Libro> libros;
}

