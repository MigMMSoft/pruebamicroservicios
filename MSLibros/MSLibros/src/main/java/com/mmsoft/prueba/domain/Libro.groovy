package com.mmsoft.prueba.domain

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table
import javax.persistence.ManyToMany
import javax.persistence.ManyToOne
import javax.persistence.CascadeType
import javax.persistence.JoinTable
import javax.persistence.JoinColumn
import javax.validation.constraints.NotNull

/**
 *
 * @author MigSoft
 */
@Entity(name = "Libro")
@Table(name = "libro")
class Libro {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id
    @NotNull
    Long version
    String nombre
    String estilo
    Date fechaCreacion
    
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "libro_publicacion", 
        joinColumns = @JoinColumn(name = "libro_publicaciones_id"), 
        inverseJoinColumns = @JoinColumn(name = "publicacion_id"))
    List<Publicacion> publicaciones
    
    @ManyToOne
    @JoinColumn(name = "autor_id")
    Autor autor
}

