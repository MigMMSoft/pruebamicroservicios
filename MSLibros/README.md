# Ejemplo Swagger DB

Este proyecto es una integraci&oacute;n de un proyecto swager mas una DB postgre (JPA) 

Prueba de relacion de DB (uno a muchos y muchos a muchos).

### Detalle del sistema

- Groovy 2.4.13
- Java 8
- Spring Boot 1.5.8.RELEASE
- Swagger 2
- Maven 3
- Hibernate 4
- DB Postgresql 9.6

### Creaci&oacute;n del proyecto

Generaci&oacute;n de c&oacute;digo Java Spring Boot

```bash
java -jar ../swagger-codegen-cli-2.2.1.jar generate -i PruebaMSLibros.yaml -l spring -o MSLibros
```

Para la prueba se requiere una base de datos postgre que se debe crear con los siguientes comandos:

```sql
--clave desarrollo
CREATE ROLE prueba_ms_libros LOGIN
  ENCRYPTED PASSWORD 'md5db59346a6ca28c5fb561d24fc5b7730d'
  SUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;

CREATE DATABASE prueba_ms_libros
  WITH ENCODING='UTF8'
       OWNER=prueba_ms_libros
       CONNECTION LIMIT=-1;


CREATE TABLE autor
(
  id bigint NOT NULL,
  version bigint NOT NULL,
  nacionalidad character varying(255) NOT NULL,
  nombre character varying(255) NOT NULL,
  CONSTRAINT autor_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE autor
  OWNER TO prueba_ms_libros;

CREATE TABLE publicacion
(
  id bigint NOT NULL,
  version bigint NOT NULL,
  fecha timestamp without time zone NOT NULL,
  orden integer NOT NULL,
  CONSTRAINT publicacion_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE publicacion
  OWNER TO prueba_ms_libros;


CREATE TABLE libro
(
  id bigint NOT NULL,
  version bigint NOT NULL,
  autor_id bigint NOT NULL,
  estilo character varying(255) NOT NULL,
  fecha_creacion timestamp without time zone NOT NULL,
  nombre character varying(255) NOT NULL,
  CONSTRAINT libro_pkey PRIMARY KEY (id),
  CONSTRAINT fk6232382dc65960f FOREIGN KEY (autor_id)
      REFERENCES autor (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE libro
  OWNER TO prueba_ms_libros;


CREATE TABLE libro_publicacion
(
  libro_publicaciones_id bigint,
  publicacion_id bigint,
  CONSTRAINT fk8cb036a01707980f FOREIGN KEY (publicacion_id)
      REFERENCES publicacion (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk8cb036a0a4fa1a63 FOREIGN KEY (libro_publicaciones_id)
      REFERENCES libro (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE libro_publicacion
  OWNER TO prueba_ms_libros;
  

CREATE SEQUENCE hibernate_sequence START 1;
```

Ejecuci&oacute;n de la aplicaci&oacute;n en distintos puertos (por defecto es 8080)

```bash
cd target
java -jar .\MSLibros-1.0.0.jar --server.port=8888
```