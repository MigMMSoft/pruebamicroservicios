# Hystrix Dashboard

Consoloa de Hystrix para monitoreo

## Detalle de los sistemas

- Java 11
- Maven 3

## Compilar y probar

```
mvn clean package
java -jar target/hystrix-dashboard-2.0.0.jar
```

## Crear de imagen y pod

Una vez compilado se crea la imagen docker y se publica el pod

```
$ docker image rm -f mmsoft.hystrix_dashboard:2.0.0

$ docker build -t mmsoft.hystrix_dashboard:2.0.0 ./ 
```

### Publica pod en el kubernetes

Genera el pod

```
$ kubectl apply -f deployment.yaml
service/hystrix-dashboard created
statefulset.apps/hystrix-dashboard created
```

ver pods

```
$ kubectl get pods
```
## Notas

Ver errores

```
$ kubectl describe pod hystrix-dashboard-0
$ kubectl logs hystrix-dashboard-0
```

## Eliminar despliegue en kubernetes

```
$ kubectl delete statefulset hystrix-dashboard
$ kubectl delete service hystrix-dashboard
```

## Referencias

Para ver la consila de monitoreo se debe indicar la URL del gateway que se quiere monitoriar


### Referencias

In your browser, go to [http://localhost:7979/](http://localhost:7979/) # port configurable in `application.yml`

On the home page is a form where you can
enter the URL for an event stream to monitor, for example (the
customers service running locally):
`http://localhost:9000/hystrix.stream`. Any app that uses
`@EnableHystrix` will expose the stream.

To aggregate many streams together you can use the
[Turbine sample](https://github.com/spring-cloud-samples/turbine).
