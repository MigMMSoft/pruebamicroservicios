package com.mmsoft.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @author MigSoft
 */
@EnableConfigServer
@SpringBootApplication
@RestController
@EnableDiscoveryClient
public class MServicioConfigApplication {

    /**
     * 
     * @param args 
     */
    public static void main(String[] args) {
        SpringApplication.run(MServicioConfigApplication.class, args);
    }

    @RequestMapping("/")
    public String home() {
        return "Micro Servicio de configuración centralizada desarrollado con spring-boot";
    }
}
