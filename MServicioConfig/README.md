# MServicioConfig

Microservicio que implementa SpringCloudConfig para manejar las configuraciones de todos los Microservicio de forma centralizada

### Detalle de los sistemas

- Java 8
- Maven 3
- Docker
- kubernetes 1.2.1

## Compilar y probar

```
mvn clean package
```

Genera en el directorio targer la **CloudConfig-2.0.0.jar**

Ejecutar servidor con 

```
java -jar ./target/CloudConfig-2.0.0.jar
```

### Detalle del Servicio

/{application}/{profile}[/{label}]
/{application}-{profile}.yml
/{label}/{application}-{profile}.yml
/{application}-{profile}.properties
/{label}/{application}-{profile}.properties

curl -X POST 127.0.0.1:8083/encrypt -d "clave configC"
configC={cipher}c176a49340a2b57641b8a41e9d7f94ee4aa980d13626e8a0eaf1fc22203f4a900123f8bda1bc9fdfb138069afe3e9c76

En kubernetes expone en el puerto 30083

## Crear pod

Una vez compilado se crea la imagen docker y se publica

```
$ docker image rm -f mmsoft.cloud_config:2.0.0

$ docker build -t mmsoft.cloud_config:2.0.0 ./ 

$docker image build -t mmsoft.cloud_config:2.0.0 .
$docker push mmsoft.cloud_config:2.0.0
```

### Publica pod en el kubernetes

Agrega la key de encriptacion (secret)

```
$ kubectl apply -f ./secret.yaml
$ kubectl get secrets
```

|NAME                |TYPE                                |DATA |AGE |
|--------------------|------------------------------------|-----|----|
|default-token-bvndz |kubernetes.io/service-account-token | 3   |66d |
|config-cm           |Opaque                              | 1   |119s|

Genera el pod

```
$ kubectl apply -f deployment.yaml
configmap/cloud-config-cm created
service/cloud-config created
deployment.apps/cloud-config created
```

ver pods

```
$ kubectl get pods
```

## Eliminar despliegue en kubernetes

```
$ kubectl delete statefulset cloud-config
$ kubectl delete service cloud-config
$ kubectl delete configmap cloud-config-cm
```

## Notas

Ver errores

```
$ kubectl describe pod cloud-config-0
$ kubectl logs cloud-config-0
```

## Ecosistema
## Externalized Configuration
**Spring Cloud Config**
Spring Cloud Config provides server and client-side support for externalized configuration in a distributed system. With the Config Server you have a central place to manage external properties for applications across all environments.


Referencia [Link](https://cloud.spring.io/spring-cloud-config/reference/html/)

## Configuracion

Git con las configuraciones de cada servicio
https://bitbucket.org/MigMMSoft/cloudconfigtest/src/master/