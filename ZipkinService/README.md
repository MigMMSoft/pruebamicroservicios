# Servidor Zipkin
 
Zipkin es un sistema de rastreo distribuido. Ayuda a recopilar los datos de sincronizaci�n necesarios para solucionar problemas de latencia en arquitecturas de microservicio. Gestiona tanto la recopilaci�n como la b�squeda de estos datos. El dise�o de Zipkin se basa en el documento de Google Dapper.

https://zipkin.io/

### Detalle de los sistemas

- Java 8

### Configuraci&oacute;n

En los clientes se deben agregar las siguiente librerias

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-sleuth</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-zipkin</artifactId>
</dependency>
```

Ademas en la clase main se debe agregar la siguiente linea

```java
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class ClasePrincipal {
	public static void main(String[] args) {
        new SpringApplicationBuilder(ClasePrincipal.class).web(true).run(args);
    }
    
    @Bean
    public AlwaysSampler defaultSampler(){
        return new AlwaysSampler();
    }
}
```

El servidor se puede bajar desde https://dl.bintray.com/openzipkin/maven/io/zipkin/java/zipkin-server/
Para la prueba de esta utilizado la versi�n 2.3.8

Para ejecutar el servidor zipkin

```bash
java -jar zipkin-server-2.8.3-exec.jar
```

El proyecto Docker Zipkin es capaz de construir im�genes acoplables. 
Para generar el contenedor esta el siguiente script YML https://github.com/openzipkin/docker-zipkin/blob/master/docker-compose.yml
Para levantar el docker se debe ejecutar la siguiente instrucci�n:

```
docker run -d -p 9411:9411 openzipkin/zipkin
```