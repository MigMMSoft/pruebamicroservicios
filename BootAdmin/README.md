# BootAdmin

Consola de administracion para spring boot

## Detalle de los sistemas

- Java 11
- Maven 3

## Compilar y probar

```
mvn clean package
```

Genera en el directorio targer la **BootAdmin-2.0.0.jar**

Ejecutar servidor con 

```
java -jar ./target/BootAdmin-2.0.0.jar
```

## Crear de imagen y pod

Una vez compilado se crea la imagen docker y se publica el pod

```
$ docker image rm -f mmsoft.boot_admin:2.0.0

$ docker build -t mmsoft.boot_admin:2.0.0 ./ 
```

### Publica pod en el kubernetes

Genera el pod

```
$ kubectl apply -f deployment.yaml
configmap/boot-admin-cm created
service/boot-admin created
statefulset.apps/boot-admin created
```

ver pods

```
$ kubectl get pods
```

## Eliminar despliegue en kubernetes

```
$ kubectl delete statefulset boot-admin
$ kubectl delete service boot-admin
$ kubectl delete configmap boot-admin-cm
```

## Notas

Ver errores

```
$ kubectl describe pod boot-admin-0
$ kubectl logs boot-admin-0
```

### Referencias

Repositorio Git del proyecto Spring Boot Admin
Referencia [Link](https://codecentric.github.io/spring-boot-admin/current/)

A Guide to Spring Boot Admin
Referencia [Link](https://www.baeldung.com/spring-boot-admin)

### Configuracion

Git con las configuraciones de cada servicio
https://bitbucket.org/MigMMSoft/cloudconfigtest/src/master/

