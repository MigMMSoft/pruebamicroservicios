package com.mmsoft.boot.admin;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author MigSoft
 */
@Configuration
@EnableAutoConfiguration
@EnableAdminServer
@EnableDiscoveryClient
@EnableEurekaClient
public class MServicioAdminApplication {

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(MServicioAdminApplication.class, args);
    }
}
