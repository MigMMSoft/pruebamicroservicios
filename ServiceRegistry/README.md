# Implementaci&oacute;n Eureka Server

Este proyecto tiene la implementaci&oacute;n de Eureka.
Registry / Discovery Service (Eureka Netflix OSS): Este componente ser&aacute; una pieza central en la arquitectura en el que cada servicio se registrar&aacute; y a su vez podr&aacute; consultar la direcci&oacute;n de los dem&aacute;s servicios para consumirlos.


## Detalle del sistema

- Java 8
- Maven 4
- Spring Boot 2.6.4
- Docker
- kubernetes 1.2.1

## Compilar y probar

```
mvn clean package
```

Genera en el directorio targer la **ServiceRegistry-2.0.0.jar**

Ejecutar servidor con 

```
java -jar ./target/ServiceRegistry-2.0.0.jar
```

## Detalle del Servicio

Los micros servicios se deben registrar en eureka en la siguiente URL:

http://localhost:8081/eureka

La consola de Eureka queda disponible en la siguiente URL:

http://localhost:8081/

## Referencias

https://spring.io/projects/spring-cloud-netflix


## Crear pod

Una vez compilado se crea la imagen docker y se publica

```
$ docker build -t mmsoft.registry:2.0.0 ./ 
```
para eliminar imagen docker
```
$ docker images --all|grep mmsoft
$ docker rmi [image id]
```


Publica pod en el kubernetes

```
$ kubectl apply -f deployment.yaml
configmap/eureka-cm created
service/eureka-lb created
service/eureka created
statefulset.apps/eureka created
```
ver pods

```
$ kubectl get pods
```

|NAME                    |READY   |STATUS    |RESTARTS   |AGE|
|------------------------|--------|----------|-----------|---|
|svclb-eureka-lb-l2pb6   |1/1     |Running   |0          |25s|
|eureka-0                |1/1     |Running   |0          |25s|

## Notas

Ver errores

$ kubectl describe pod eureka-0
$ kubectl logs eureka-0 -f

## Eliminar despliegue en kubernetes

kubectl delete configmap eureka-cm
kubectl delete service eureka-lb
kubectl delete service eureka
kubectl delete statefulset eureka

## Crea imagen docker

$ kubectl create configmap eureka-cm -o=yaml > deployment.yaml

$ kubectl create service clusterip eureka-lb --tcp=8081:8081 --dry-run=client -o=yaml >> deployment.yaml

$ kubectl create deployment registry --image=mmsoft.registry --dry-run=client -o=yaml > deployment.yaml


