# PruebaMicroServicios

Proyecto de ejemplo de implementaci�n de MicroServicios utilizando API de Netflix y SpringBoot

# Detalle de los sistemas

- Java 8
- Spring Boot
- Maven 3
- kubernetes 1.2.1

# Ecosistema

## Service Registry
**Eureka**
Eureka is a REST (REpresentational State Transfer) based service that is primarily used in the AWS cloud for locating services for the purpose of load balancing and failover of middle-tier servers.

## Load Balancer
**Ribbon**
Ribbon allows load balancing among a static list of instances that are declared, or however many instances of the service that are discovered from a registry lookup.

## Circuit Breaker
**Hystrix**
Hystrix is a latency and fault tolerance library designed to isolate points of access to remote systems, services and 3rd party libraries, stop cascading failure and enable resilience in complex distributed systems where failure is inevitable.

Hystrix implements both the circuit breaker and bulkhead patterns.

## Externalized Configuration
**Spring Cloud Config**
Spring Cloud Config provides server and client-side support for externalized configuration in a distributed system. With the Config Server you have a central place to manage external properties for applications across all environments.

## Proxy/Routing
**Zuul**
Zuul is an edge service that provides dynamic routing, monitoring, resiliency, security, and more. Zuul supports multiple routing models, ranging from declarative URL patterns mapped to a destination, to groovy scripts that can reside outside the application archive and dynamically determine the route.

## Distributed Tracing
**Sleuth/Zipkin**
Spring Cloud Sleuth generates trace IDs for every call and span IDs at the requested points in an application. This information can be integrated with a logging framework to help troubleshoot the application by following the log files, or broadcast to a Zipkin server and stored for analytics and reports.

Referencia [Link](https://access.redhat.com/documentation/en-us/reference_architectures/2017/html-single/spring_boot_microservices_on_red_hat_openshift_container_platform_3/index)

# Orden de ejecucion

## Base del sistema

Registro de servicios, Configuracion Central y ApiGateway

